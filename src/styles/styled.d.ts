// import original module declarations
import 'styled-components'

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    fonts: {
      primary: string
    }
    colors: {
      surface: string
      elevated: string
      primary: string
      white: string
      grey: string
      colorAccent900: string
      colorAccent800: string
      colorAccent700: string
      colorAccent600: string
      colorAccent500: string
      colorAccent400: string
      colorAccent300: string
      colorAccent200: string
      colorAccent100: string
      colorAccent50: string
    }
  }
}
