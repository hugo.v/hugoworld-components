/* eslint-disable no-unused-vars */
// my-theme.ts
import { DefaultTheme } from 'styled-components'

const theme: DefaultTheme = {
  fonts: {
    primary: 'Rubik'
  },
  colors: {
    surface: '#222831',
    elevated: '#393E46',
    primary: '#FD7014',
    white: '#EEEEEE',
    grey: '#A7B1C1',
    colorAccent900: '#d85100',
    colorAccent800: '#e26a00',
    colorAccent700: '#e87a00',
    colorAccent600: '#ee8902',
    colorAccent500: '#f29404',
    colorAccent400: '#f3a327',
    colorAccent300: '#f5b44d',
    colorAccent200: '#f8c97f',
    colorAccent100: '#fbdeb2',
    colorAccent50: 'fdf2e0;'
  }
}

export { theme }
