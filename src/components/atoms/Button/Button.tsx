import * as React from 'react'
import styled, { ThemeProvider } from 'styled-components'

import { theme } from '../../../styles/theme'

export type ButtonProps = {
  label: string
  color: string
  variant?: string
  size?: string
  onClick?: Function
}

const getSizePadding = (size: string) => {
  switch (size) {
    case 'large': {
      return '.5rem 1rem'
    }
    case 'regular': {
      return '0.375rem 0.75rem'
    }
    case 'small': {
      return '.25rem .5rem'
    }
    default: {
      return '0.375rem 0.75rem'
    }
  }
}

const getFontSize = (size: string) => {
  switch (size) {
    case 'large': {
      return '1.25rem'
    }
    case 'regular': {
      return '1rem'
    }
    case 'small': {
      return '.875rem;'
    }
    default: {
      return '1rem'
    }
  }
}

const ButtonStyled = styled.button`
  display: inline-block;
  font-weight: 400;
  text-align: center;
  white-space: nowrap;
  user-select: none;
  border: 1px solid transparent;
  padding: ${(p: ButtonProps) => getSizePadding(p.size)};
  font-size: ${(p: ButtonProps) => getFontSize(p.size)};
  line-height: 1.5;
  border-radius: 0.25rem;
  color: #fff;
  display: block;
  position: relative;
  border: 2px solid ${(p) => p.color};
  transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;

  &:active {
    transform: translateY(5px);
  }

  &:before {
    display: block;
    position: absolute;
    left: 0px;
    bottom: 0px;
    height: 0px;
    width: 100%;
    z-index: -1;
    content: '';
    color: #000 !important;
    transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
  }

  &:hover:before {
    top: 0%;
    bottom: auto;
    height: 100%;
  }

  &:not(:disabled):not(.disabled) {
    cursor: pointer;
  }
`

const Primary = styled(ButtonStyled)`
  background-color: ${(p) => p.color};

  &:before {
    background: transparent;
  }

  &:hover {
    filter: brightness(115%);
    text-shadow: ntwo;
  }
`

const Secondary = styled(ButtonStyled)`
  background-color: transparent;

  &:before {
    background: ${(p) => p.color};
  }

  &:hover {
    background-color: transparent;
    text-shadow: ntwo;
  }
`

const variants = {
  primary: Primary,
  secondary: Secondary
}

const Button = ({ label, color, variant, size, onClick }: ButtonProps) => {
  const Variant = variants[variant] || variants.primary

  return (
    <ThemeProvider theme={theme}>
      <Variant onClick={onClick} color={color} size={size}>
        {label}
      </Variant>
    </ThemeProvider>
  )
}

export default Button
