import * as React from 'react'
import styled, { ThemeProvider, css } from 'styled-components'

import { theme } from '../../../styles/theme'

export type HeaderProps = {
  children: JSX.Element | string | undefined
  as: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
}

const headerStyles = css`
  display: inline;
  color: white;
  font-weight: 300;
  background-image: linear-gradient(
    120deg,
    ${(p) => p.theme.colors.colorAccent600} 0%,
    ${(p) => p.theme.colors.colorAccent800} 100%
  );
  background-repeat: no-repeat;
  background-size: 100% 0.25em;
  background-position: 0 90%;
  transition: background-size 0.25s ease-in;
`

const H1 = styled.h1`
  ${headerStyles}
  color: white;
`
const H2 = styled.h2`
  ${headerStyles}
  color: white;
`
const H3 = styled.h3`
  ${headerStyles}
  color: white;
`
const H4 = styled.h4`
  ${headerStyles}
  color: white;
`
const H5 = styled.h5`
  ${headerStyles}
  color: white;
`
const H6 = styled.h6`
  ${headerStyles}
  color: white;
`

const getHeader = (as: string) => {
  switch (as) {
    case 'h1': {
      return H1
    }
    case 'h2': {
      return H2
    }
    case 'h3': {
      return H3
    }
    case 'h4': {
      return H4
    }
    case 'h5': {
      return H5
    }
    case 'h6': {
      return H6
    }
    default: {
      return H1
    }
  }
}

const Header = ({ children, as }: HeaderProps) => {
  const Head = getHeader(as)
  return (
    <ThemeProvider theme={theme}>
      <Head>{children}</Head>
    </ThemeProvider>
  )
}

export default Header
