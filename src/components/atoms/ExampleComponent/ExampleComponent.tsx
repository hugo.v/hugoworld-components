import * as React from 'react'
import styled, { ThemeProvider } from 'styled-components'

import { theme } from '../../../styles/theme'

const Wrapper = styled.div`
  padding: 4em;
  background: ${(props) => props.theme.colors.surface};
`

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: ${(props) => props.theme.colors.white};
  font-family: ${(props) => props.theme.fonts.primary};
`

export type ExampleComponentProps = {
  text: string
}

const ExampleComponent = ({ text }: ExampleComponentProps) => {
  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        <Title>{text}</Title>
      </Wrapper>
    </ThemeProvider>
  )
}

export default ExampleComponent
