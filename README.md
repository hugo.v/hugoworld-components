# hugoworld-components

> hugoworld components library

[![NPM](https://img.shields.io/npm/v/hugoworld-components.svg)](https://www.npmjs.com/package/hugoworld-components) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
$> npm install --save hugoworld-components
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'hugoworld-components'
import 'hugoworld-components/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## Development

### Installation

```bash
# Installation
$> npm install
$> cd storybook
$> npm install
```

### Start

```bash
# Launch dev environment, in one bash instance:
$> npm start

# in another:
$> cd storybook
$> npm start
```

You get free hot reloading !

## License

MIT © [Krakcen](https://github.com/Krakcen)
