/* eslint-disable no-unused-vars */
import React from 'react'
import { Meta, Story } from '@storybook/react'

import { ExampleComponent, ExampleComponentProps } from 'hugoworld-components'

export default {
  title: 'Samples/ExampleComponent',
  component: ExampleComponent,
  argTypes: {
    text: { control: 'text' },
  },
} as Meta

const Template: Story<ExampleComponentProps> = (args) => <ExampleComponent {...args} />;

export const HelloWorld = Template.bind({});
HelloWorld.args = {
  text: "Hello World",
};

const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consequat ipsum mollis ante laoreet egestas. Duis a est id neque faucibus condimentum. In ac fermentum est. Integer ut porttitor nisi. Suspendisse sed velit vehicula, vehicula magna at, congue tortor. Suspendisse et nibh nulla. Vivamus ac diam auctor, malesuada mi id, bibendum sapien. Curabitur ullamcorper pellentesque lorem, et venenatis mi. Suspendisse eu eleifend ipsum. Nullam faucibus vel risus vitae pulvinar. Ut non dolor imperdiet, tincidunt odio a, feugiat velit. Aliquam gravida mauris eu maximus laoreet. Sed sem neque, efficitur sed pretium at, feugiat suscipit libero. Proin sodales lorem vitae ex maximus, a bibendum ante vehicula.'
export const Lorem = Template.bind({});
Lorem.args = {
  text: lorem,
};
