/* eslint-disable no-unused-vars */
import React from 'react'
import { Meta, Story } from '@storybook/react'

import { Header, HeaderProps } from 'hugoworld-components'

export default {
  title: 'Atoms/Header',
  component: Header,
  argTypes: {
    children: {
      control: "text",
    },
    as: {
      options: ["h1", "h2", "h3", "h4", "h5", "h6"],
      control: "select",
    },
  },
} as Meta

const Template: Story<HeaderProps> = (args) => <Header {...args} />;

export const H1 = Template.bind({});
H1.args = {
  children: "H1 Header",
  as: "h1",
};

export const H2 = Template.bind({});
H2.args = {
  children: "H2 Header",
  as: "h2",
};

export const H3 = Template.bind({});
H3.args = {
  children: "H3 Header",
  as: "h3",
};

export const H4 = Template.bind({});
H4.args = {
  children: "H4 Header",
  as: "h4",
};

export const H5 = Template.bind({});
H5.args = {
  children: "H5 Header",
  as: "h5",
};

export const H6 = Template.bind({});
H6.args = {
  children: "H6 Header",
  as: "h6",
};
