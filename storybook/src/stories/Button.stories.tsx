/* eslint-disable no-unused-vars */
import React from 'react'
import { Meta, Story } from '@storybook/react'

import { Button, ButtonProps } from 'hugoworld-components'

export default {
  title: 'Atoms/Button',
  component: Button,
  argTypes: {
    label: { control: 'text' },
  },
} as Meta

const argTypes = {
  label: {
    control: "text",
  },
  color: {
    control: "color",
  },
  variant: {
    options: ["primary", "secondary"],
    control: "select",
  },
  size: {
    options: ["small", "regular", "large"],
    control: "select",
  }
}

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

const SizeTemplate: Story<ButtonProps> = (args) =>
  <div style={{ display: "inline-flex" }}>
    <span style={{ marginRight: "1rem" }}><Button {...args} variant="primary"/></span>
    <Button {...args} variant="secondary"/>
  </div>

export const Primary = Template.bind({});
Primary.argTypes = argTypes;
Primary.args = {
  label: "Primary Button",
  color: "#e26a00",
  variant: "primary",
};

export const Secondary = Template.bind({});
Secondary.argTypes = argTypes;
Secondary.args = {
  label: "Secondary Button",
  color: "#e26a00",
  variant: "secondary",
};

export const Small = SizeTemplate.bind({});
Small.argTypes = argTypes;
Small.args = {
  label: "Small Button",
  color: "#e26a00",
  size: "small",
};

export const Regular = SizeTemplate.bind({});
Regular.argTypes = argTypes;
Regular.args = {
  label: "Regular Button",
  color: "#e26a00",
  size: "regular",
};

export const Large = SizeTemplate.bind({});
Large.argTypes = argTypes;
Large.args = {
  label: "Large Button",
  color: "#e26a00",
  size: "large",
};
